import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class ImageMNIST {
    
    public final static int width = 28;
    public final static int height = 28;    
    public byte[] pixels;
    public int label;
    
    public ImageMNIST(byte[] pixels, int label){
        this.pixels = pixels;
        this.label = label;
    }
    
    public BufferedImage constructImage(){
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
        byte[] array = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(pixels, 0, array, 0, array.length);
        return image;
    }
}
