import java.util.Arrays;
import java.util.Random;

public class Network {
    
    private int num_of_layers;
    private int[] sizes;
    private double[][] biases;
    private double[][][] weights;
    
    //Back propogation required activations
    private double[][] activations;
    private double[][] product_sums;
    
    // RNG
    Random random = new Random();
    
    // Training the network
    private double[][] training_data;
    private double learning_rate;
    private int batch_size = 0;
    private int curr_epoch = 0;
    private int epochs = 0;
    
    
    public Network(int[] sizes_){
        if(sizes_.length < 3){
            throw new IllegalArgumentException("Invalid input length");
        }
        
        num_of_layers = sizes_.length;
        this.sizes = sizes_;
        
        System.out.println("Creating a network");
        
        biases = new double[num_of_layers-1][];
        for(int i = 0; i < num_of_layers-1; i++){
            biases[i] = new double[sizes[i+1]];
            for(int j = 0 ; j < biases[i].length; j++){
                biases[i][j] = random.nextGaussian();
            }
        }
        
        weights = new double[sizes.length-1][][];
        for(int i = 1; i < sizes.length; i++){
            weights[i-1] = new double[sizes[i]][sizes[i-1]];
            for(int j = 0; j < sizes[i]; j++){
                for(int z = 0; z < sizes[i-1]; z++){
                    weights[i-1][j][z] = random.nextGaussian();
                }
            }
        }
    }
    
    public void SGD(double[][] training_data, double learning_rate, int batch_size, int epochs){
        this.training_data = training_data;
        this.learning_rate = learning_rate;
        this.batch_size = batch_size;
        this.epochs = epochs;
//        for(int epoch_count = 0; epoch_count < epochs; epoch_count++){
//            shuffleArray(training_data);
//            for(int i = 1; i*batch_size <= training_data.length; i++){
//                double[][] training_batch = Arrays.copyOfRange(training_data, (i-1)*batch_size, i*batch_size);
//                runBatch(training_batch, learning_rate);
//            }
//            // TEST
//            System.out.println("Epoch #"+epoch_count);
//        }
    }
    
    public void runEpoch(){
        if(curr_epoch >= epochs)
            return;
        //TEST
        System.out.println("Epoch #"+curr_epoch);
        shuffleArray(training_data);
        for(int i = 1; i*batch_size <= training_data.length; i++){
            double[][] training_batch = Arrays.copyOfRange(training_data, (i-1)*batch_size, i*batch_size);
            runBatch(training_batch, learning_rate);
        }
        curr_epoch++;
    }
    
    public boolean canRunEpoch(){
        return curr_epoch < epochs;
    }
    
    /**
     * Pushes a given input through the network generating activation and product sum matrices.
     * @param input_ data going into the input layer
     * @return networks output
     */
    public double[] feedforward(double[] input_){        
        activations = new double[num_of_layers][];
        activations[0] = input_;
        product_sums = new double[num_of_layers-1][];
        
        for(int i = 0; i < num_of_layers-1; i++){
            product_sums[i] = vectorSum(matrixVectorProduct(weights[i], activations[i]), biases[i]);
            activations[i+1] = sigmoid(product_sums[i]);
        }
        return activations[activations.length-1];
    }
    
    /**
     * Calculates the error for the output layer and then propogates it backwards throughout
     * the networl.
     * @param expected_output expected result at the output layer
     * @return a matrix of errors [layer][errors]
     */
    public double[][] backPropogateError(double[] expected_output){
        double[][] error = new double[num_of_layers-1][];
        error[error.length-1] = hadamardProduct(vectorDiff(activations[activations.length-1], expected_output),
                sigmoidPrime(product_sums[product_sums.length-1]));
        
        for(int i = error.length-2; i >=0; i--){
            error[i] = hadamardProduct(tMatrixVectorProduct(weights[i+1], error[i+1]), sigmoidPrime(product_sums[i]));
        }
        return error;
    }
    
    public void runBatch(double[][] batch, double learning_rate){
        
        double[][][] weight_sums = new double[num_of_layers-1][][];
        double[][] bias_sums = new double[num_of_layers-1][];
        for(int i = 0; i < batch.length; i++){
            double[] input = Arrays.copyOfRange(batch[i], 0, sizes[0]);
            double[] expected_output = Arrays.copyOfRange(batch[i], sizes[0], batch[i].length);
            
            feedforward(input);
            double[][] error = backPropogateError(expected_output);
            for(int j = num_of_layers-2; j >= 0; j--){
                if(weight_sums[j] == null)
                    weight_sums[j] = tVectorProduct(error[j], activations[j]);
                else
                    matrixSum(weight_sums[j], tVectorProduct(error[j], activations[j]));
                
                if(bias_sums[j] == null)
                    bias_sums[j] = error[j];
                else
                    bias_sums[j] = vectorSum(bias_sums[j], error[j]); // optimize needed
            }
        }
        
        double weighted_learn_rate = learning_rate/batch.length;
        for(int i = 0; i < num_of_layers-1; i++){
            vectorDiff(biases[i], bias_sums[i], weighted_learn_rate);
            matrixDiff(weights[i], weight_sums[i], weighted_learn_rate);
        }
    }
    
    public double[] sigmoid(double[] z){
        double[] s_z = new double[z.length];
        for(int i = 0; i < z.length; i++){
            s_z[i] = 1.0/(1.0+Math.exp(-z[i]));
        }
        return s_z;
    }
    
    public double[] sigmoidPrime(double[] z){
        double[] s_p = sigmoid(z);
        for(int i = 0; i < s_p.length; i++){
            s_p[i] = s_p[i]*(1.0-s_p[i]);
        }
        return s_p;
    }
    
    // =======================
    // === Utility Methods ===
    // =======================
    
    // used in feedforward
    
    public double[] matrixVectorProduct(double[][] m, double[] v){
        if(m[0].length != v.length)
            throw new IllegalArgumentException("m columns != v rows -> matrix vector product");
        
        double[] product =new double[m.length];
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < v.length; j++){
                product[i] += m[i][j]*v[j];
            }
        }
        return product;
    }
    
    public double[] vectorSum(double[] v1, double[] v2){
        if(v1.length != v2.length)
            throw new IllegalArgumentException("v1.lenght != v2.length -> vector sum");
        double[] vectorSum = new double[v1.length];
        for(int i = 0; i < vectorSum.length; i++){
            vectorSum[i] = v1[i]+v2[i];
        }
        return vectorSum;
    }
    //--- end of feedforward methods
    
    // used in backpropogation
    public double[] vectorDiff(double[] v1, double[] v2){
        if(v1.length != v2.length)
            throw new IllegalArgumentException("v1.lenght != v2.length -> vector difference");
        double[] diff = new double[v1.length];
        for(int i = 0; i < diff.length; i++){
            diff[i] = v1[i] - v2[i];
        }
        return diff;
    }
    
    public double[] hadamardProduct(double[] v1, double[] v2){
        if(v1.length != v2.length)
            throw new IllegalArgumentException("v1.lenght != v2.length -> hadamard product");
        double[] h_product = new double[v1.length];
        for(int i = 0; i < h_product.length; i++){
            h_product[i] = v1[i]*v2[i];
        }
        return h_product;
    }
    
    /**
     * Calculates the product of transpose(m matrix) and vector v
     * @param m matrix
     * @param v vector
     * @return transpose(m)*v
     */
    public double[] tMatrixVectorProduct(double[][] m, double[] v){
        if(m.length != v.length)
            throw new IllegalArgumentException("m columns != v rows -> transpose matrix vector product");
        
        double[] product = new double[m[0].length];
        for(int i = 0; i < product.length; i++){
            for(int j = 0; j < m.length; j++){
                product[i] += m[j][i]*v[j];
            }
        }
        return product;
    }
    //--- end of backpropogation methods
    
    // used in gradient descent
    /**
     * Calculates the product of v1 and transpose(v2)
     * @param v1 vector
     * @param v2 vector
     * @return v1*transpose(v2)
     */
    public double[][] tVectorProduct(double[] v1, double[] v2){
        double[][] product = new double[v1.length][v2.length];
        for(int i = 0; i < v1.length; i++){
            for(int j = 0; j < v2.length; j++){
                product[i][j] = v1[i]*v2[j];
            }
        }
        return product;
    }
    
    /**
     * Adds m2 to matrix m1.
     * @param m1 matrix
     * @param m2 matrix
     */
    public void matrixSum(double[][] m1, double[][] m2){
        for(int i = 0; i < m1.length; i++){
            for(int j = 0; j < m1[0].length; j++){
                m1[i][j] += m2[i][j];
            }
        }
    }
    
    public void matrixDiff(double[][] m1, double[][] m2, double learn_rate){
        for(int i = 0; i< m1.length; i++){
            for(int j = 0; j < m1[0].length; j++){
                m1[i][j] -= learn_rate*m2[i][j];
            }
        }
    }
    
    public void vectorDiff(double[] v1, double[] v2, double learn_rate){
        if(v1.length != v2.length)
            throw new IllegalArgumentException("v1.lenght != v2.length -> vector difference");
        for(int i = 0; i < v1.length; i++){
            v1[i] -= learn_rate*v2[i];
        }
    }
    //--- end of gradient descent methods
    
    public <V> void shuffleArray(V[] arr){
        int index;
        V temp;
        for(int i = arr.length-1; i > 0; i--){
            index = random.nextInt(i+1);
            temp = arr[index];
            arr[index] = arr[i];
            arr[i] = temp;
        }
    }
}
