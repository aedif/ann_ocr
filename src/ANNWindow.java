import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListModel;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.JFileChooser;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JList;
import javax.swing.DefaultListModel;

public class ANNWindow {
    
    Network n;

    private JFrame frame;
    private JTextField txtNetwork;
    private JTextField txtLearnRate;
    private JTextField txtBatchSize;
    private JTextField txtEpoch;
    private JButton btnStart;
    private JButton btnStop;
    private JButton btnTestLabel;
    private JButton btnImageTest;
    private JButton btnLoadImage;
    private JButton btnLoadLabel;
    
    private Thread sgd_thread;
    private final JFileChooser fc = new JFileChooser();
    private File image_file;
    private File label_file;
    private ImageMNIST[] images;
    private File test_image_file;
    private File test_label_file;
    private ImageMNIST[] test_images;
    private double[][] test_data;
    private JLabel[] image_labels;
    private JList[] image_lists;
    private JLabel lblInfoText;
    
    private int[] correct_last_counts;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    ANNWindow window = new ANNWindow();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public ANNWindow() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        fc.setCurrentDirectory(new File("C:\\Users\\Aedifico\\Desktop\\pend"));
        fc.setDialogTitle("Select MNIST file.");
        
        image_file = null;
        label_file = null;
        
        frame = new JFrame();
        frame.setBounds(100, 100, 774, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(new BorderLayout(0, 0));
        
        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.NORTH);
        
        btnStart = new JButton("Start");
        btnStart.setEnabled(false);
        btnStart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                start();
            }
        });
        panel.setLayout(new GridLayout(0, 4, 0, 0));
        panel.add(btnStart);
        
        btnStop = new JButton("Stop");
        btnStop.setEnabled(false);
        btnStop.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stop();
            }
        });
        panel.add(btnStop);
        
        btnLoadImage = new JButton("IMG");
        btnLoadImage.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fc.setDialogTitle("Select MNIST image file.");
                int returnVal = fc.showOpenDialog(frame);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    image_file = file;
                    if(image_file != null && label_file != null){
                        loadImages();
                    }
                } else {
                    //log.append("Open command cancelled by user." + newline);
                }
            }
        });
        panel.add(btnLoadImage);
        
        btnLoadLabel = new JButton("LBL");
        btnLoadLabel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fc.setDialogTitle("Select MNIST label file.");
                int returnVal = fc.showOpenDialog(frame);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    label_file = file;
                    if(image_file != null && label_file != null){
                        loadImages();
                    }
                } else {
                    //log.append("Open command cancelled by user." + newline);
                }
            }
        });
        panel.add(btnLoadLabel);
        
        JLabel lblEmpty = new JLabel("");
        lblEmpty.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblEmpty);
        
        JLabel lblTest = new JLabel("Test Data:");
        lblTest.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblTest);
        
        btnImageTest = new JButton("IMG");
        btnImageTest.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fc.setDialogTitle("Select MNIST test image file.");
                int returnVal = fc.showOpenDialog(frame);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    test_image_file = file;
                    if(test_image_file != null && test_label_file != null){
                        loadTestImages();
                    }
                } else {
                    //log.append("Open command cancelled by user." + newline);
                }
            }
        });
        panel.add(btnImageTest);
        
        btnTestLabel = new JButton("LBL");
        btnTestLabel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fc.setDialogTitle("Select MNIST test label file.");
                int returnVal = fc.showOpenDialog(frame);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    test_label_file = file;
                    if(test_image_file != null && test_label_file != null){
                        loadTestImages();
                    }
                } else {
                    //log.append("Open command cancelled by user." + newline);
                }
            }
        });
        panel.add(btnTestLabel);
        
        JLabel lblNetwork = new JLabel("Network: ");
        lblNetwork.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblNetwork);
        
        txtNetwork = new JTextField();
        txtNetwork.setText("[784,30,10]");
        panel.add(txtNetwork);
        txtNetwork.setColumns(10);
        
        JLabel lblLearnrate = new JLabel("Learn_rate: ");
        lblLearnrate.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblLearnrate);
        
        txtLearnRate = new JTextField();
        txtLearnRate.setText("3");
        panel.add(txtLearnRate);
        txtLearnRate.setColumns(4);
        
        JLabel lblBatchSize = new JLabel("Batch size: ");
        lblBatchSize.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblBatchSize);
        
        txtBatchSize = new JTextField();
        txtBatchSize.setText("10");
        panel.add(txtBatchSize);
        txtBatchSize.setColumns(4);
        
        JLabel lblEpochs = new JLabel("Epochs: ");
        lblEpochs.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblEpochs);
        
        txtEpoch = new JTextField();
        txtEpoch.setText("5");
        panel.add(txtEpoch);
        txtEpoch.setColumns(3);
        
        JLabel lblInfo = new JLabel("Info: ");
        lblInfo.setHorizontalAlignment(SwingConstants.TRAILING);
        panel.add(lblInfo);
        
        lblInfoText = new JLabel("epoch #, correct %");
        panel.add(lblInfoText);
        
        JPanel panel_1 = new JPanel();
        frame.getContentPane().add(panel_1, BorderLayout.CENTER);
        panel_1.setLayout(new GridLayout(2, 10, 0, 0));
        
        JLabel lblImage0 = new JLabel("0");
        lblImage0.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage0);
        
        JLabel lblImage1 = new JLabel("1");
        lblImage1.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage1);
        
        JLabel lblImage2 = new JLabel("2");
        lblImage2.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage2);
        
        JLabel lblImage3 = new JLabel("3");
        lblImage3.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage3);
        
        JLabel lblImage4 = new JLabel("4");
        lblImage4.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage4);
        
        JLabel lblImage5 = new JLabel("5");
        lblImage5.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage5);
        
        JLabel lblImage6 = new JLabel("6");
        lblImage6.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage6);
        
        JLabel lblImage7 = new JLabel("7");
        lblImage7.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage7);
        
        JLabel lblImage8 = new JLabel("8");
        lblImage8.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage8);
        
        JLabel lblImage9 = new JLabel("9");
        lblImage9.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblImage9);
        
        image_labels = new JLabel[10];
        image_labels[0] = lblImage0;
        image_labels[1] = lblImage1;
        image_labels[2] = lblImage2;
        image_labels[3] = lblImage3;
        image_labels[4] = lblImage4;
        image_labels[5] = lblImage5;
        image_labels[6] = lblImage6;
        image_labels[7] = lblImage7;
        image_labels[8] = lblImage8;
        image_labels[9] = lblImage9;
        
        JList<String> lstImage0 = new JList<String>();
        lstImage0.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage0);
        
        JList<String> lstImage1 = new JList<String>();
        lstImage1.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage1);
        
        JList<String> lstImage2 = new JList<String>();
        lstImage2.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage2);
        
        JList<String> lstImage3 = new JList<String>();
        lstImage3.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage3);
        
        JList<String> lstImage4 = new JList<String>();
        lstImage4.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage4);
        
        JList<String> lstImage5 = new JList<String>();
        lstImage5.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage5);
        
        JList<String> lstImage6 = new JList<String>();
        lstImage6.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage6);
        
        JList<String> lstImage7 = new JList<String>();
        lstImage7.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage7);
        
        JList<String> lstImage8 = new JList<String>();
        lstImage8.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage8);
        
        JList<String> lstImage9 = new JList<String>();
        lstImage9.setModel(new DefaultListModel<String>());
        panel_1.add(lstImage9);
        
        image_lists = new JList[10];
        image_lists[0] = lstImage0;
        image_lists[1] = lstImage1;
        image_lists[2] = lstImage2;
        image_lists[3] = lstImage3;
        image_lists[4] = lstImage4;
        image_lists[5] = lstImage5;
        image_lists[6] = lstImage6;
        image_lists[7] = lstImage7;
        image_lists[8] = lstImage8;
        image_lists[9] = lstImage9;
        
        for(int i = 0; i < 10; i++)
            ((DefaultListModel<String>) image_lists[i].getModel()).addElement("Correct/Total");
    }
    
    public double[] stringToDoubleArr(String str){
        String[] items = str.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
        
        double[] results = new double[items.length];
        
        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Double.parseDouble(items[i]);
            } catch (NumberFormatException nfe) {
                //NOTE: write something here if you need to recover from formatting errors
            };
        }
        return results;
    }
    
    public int[] stringToIntArr(String str){
        String[] items = str.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
        
        int[] results = new int[items.length];
        
        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {
                //NOTE: write something here if you need to recover from formatting errors
            };
        }
        return results;
    }
    
    private void loadImages(){
        if(image_file == null || label_file == null){
            btnStart.setEnabled(false);
            btnStop.setEnabled(false);
            return;
        }
        System.out.println("here");
        try {
            images = ParseMNIST.parseFiles(image_file, label_file);
            for(int i =0; i < 10; i++){
                int j = 0;
                while(images[j].label != i){
                    j++;
                }
                image_labels[i].setIcon(new ImageIcon(images[j].constructImage()));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        btnStart.setEnabled(true);
    }
    
    private void loadTestImages(){
        if(test_image_file == null || test_label_file == null){
            return;
        }
        
        correct_last_counts = new int[10];
        
        try {
            System.out.println("Loading test images");
            test_images = ParseMNIST.parseFiles(test_image_file, test_label_file);
            // convert image data to be processable by the Network
            test_data = new double[test_images.length][];
            for(int i = 0; i < test_images.length; i++){
                test_data[i] = new double[ImageMNIST.width*ImageMNIST.height];
                byte[] pixels = test_images[i].pixels;
                for(int j = 0; j < pixels.length; j++){
                    test_data[i][j] = (pixels[j] & 0xFF)/255d;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void start(){
        if(images == null)
            return;
        
        for(int i = 0; i < 10; i++){
            ((DefaultListModel<String>) image_lists[i].getModel()).clear();
            ((DefaultListModel<String>) image_lists[i].getModel()).addElement("Correct/Total");
        }
        
        // convert image data to be processable by the Network
        double[][] training_data = new double[images.length][];
        boolean printOne = true;
        for(int i = 0; i < images.length; i++){
            training_data[i] = new double[ImageMNIST.width*ImageMNIST.height+10];
            byte[] pixels = images[i].pixels;
            for(int j = 0; j < pixels.length; j++){
                training_data[i][j] = (pixels[j] & 0xFF)/255d;
            }
            training_data[i][pixels.length+images[i].label] = 1.0;
            // Test
            if(printOne){
                printOne = false;
                System.out.println("Image label = " + images[0].label);
                for(int k = 0; k < 10; k++){
                    System.out.print(training_data[0][pixels.length+k] +", ");
                }
            }
        }
        
        if(sgd_thread != null)
            sgd_thread.interrupt();
        
        sgd_thread = new Thread(new Runnable(){
            @Override
            public void run() {
                n = new Network(stringToIntArr(txtNetwork.getText()));
                n.SGD(training_data, Integer.parseInt(txtLearnRate.getText()), Integer.parseInt(txtBatchSize.getText()), Integer.parseInt(txtEpoch.getText()));
                int epoch = 1;
                while(n.canRunEpoch() && !Thread.currentThread().isInterrupted()){
                    n.runEpoch();
                    if(test_images != null){
                        runTest(epoch);
                    }
                    epoch++;
                }
                stop();
            }
        });
        sgd_thread.start();
        btnStart.setEnabled(false);
        btnStop.setEnabled(true);
        btnTestLabel.setEnabled(false);;
        btnImageTest.setEnabled(false);
        btnLoadImage.setEnabled(false);
        btnLoadLabel.setEnabled(false);
    }
    
    private void runTest(int epoch){
        if(test_images == null)
            return;
        
        int[] correct_num_counts = new int[10];
        int[] total_counts = new int[10];
        for(int i = 0; i < test_images.length; i++){
            double[] output = n.feedforward(test_data[i]);
            int highest = 0;
            double h_val = 0.0;
            for(int j = 0; j < 10; j++){
                if(output[j] > h_val){
                    h_val = output[j];
                    highest = j;
                }
            }
            if(highest == test_images[i].label)
                correct_num_counts[test_images[i].label]++;
            total_counts[test_images[i].label]++;
            
            //test
            image_labels[test_images[i].label].setIcon(new ImageIcon(test_images[i].constructImage()));
        }
        
        // TEST results
        int total_correct = 0;
        for(int i = 0; i < 10; i++){
            total_correct += correct_num_counts[i];
            System.out.println(i+": " + correct_num_counts[i] + "/" + total_counts[i] + " = " + (correct_num_counts[i]/(float)total_counts[i]));
            
            String element = "";
            DefaultListModel<String> dlm  = (DefaultListModel<String>)image_lists[i].getModel();
            if(dlm.size() > 1){
                element = "";
                int correct_diff = correct_num_counts[i] - correct_last_counts[i];
                if(correct_diff > 0)
                    element += "+" + correct_diff;
                else if(correct_diff == 0)
                    element += '-';
                else
                    element += correct_diff;
                element += "/-";
                correct_last_counts[i] += correct_diff;
            } else {
                correct_last_counts[i] = correct_num_counts[i];
                element = correct_num_counts[i] + "/" + total_counts[i];
            }
            String wr_element = element;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    dlm.addElement(wr_element);
                }
            });
        }
        
        
        System.out.println("Total: " + total_correct + "/" + test_images.length + " = " + (total_correct/(float)test_images.length));
        lblInfoText.setText("Epoch #" + epoch + " , correct " + ((total_correct/(float)test_images.length)*100) + "%");
    }
    
    private void stop(){
        if(sgd_thread != null){
            sgd_thread.interrupt();
            sgd_thread = null;
        }
        
        if(image_file != null && label_file != null)
            btnStart.setEnabled(true);
        else
            btnStart.setEnabled(false);
        btnStop.setEnabled(false);
        btnTestLabel.setEnabled(true);;
        btnImageTest.setEnabled(true);
        btnLoadImage.setEnabled(true);
        btnLoadLabel.setEnabled(true);
    }

}
