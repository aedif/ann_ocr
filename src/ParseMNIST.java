import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ParseMNIST {
    public static ImageMNIST[] parseFiles(File image_file, File label_file) throws IOException{
        DataInputStream dis = new DataInputStream(new FileInputStream(image_file));
        byte[] image_data = new byte[(int)image_file.length()];
        dis.readFully(image_data);
        dis.close();
        
        dis = new DataInputStream(new FileInputStream(label_file));
        byte[] label_data = new byte[(int)label_file.length()];
        dis.readFully(label_data);
        dis.close();
        
        int[] labels = new int[label_data.length-8];
        for(int i = 8; i < label_data.length; i++)
            labels[i-8] = label_data[i] & 0xFF;
        
        int rows = (image_data[11] & 0xFF) | 
                   ((image_data[10] & 0xFF)<< 8)| 
                   ((image_data[9] & 0xFF)<< 16)| 
                   ((image_data[8] & 0xFF) << 24);
        
        int columns = (image_data[15] & 0xFF) | 
                      ((image_data[14] & 0xFF)<< 8)| 
                      ((image_data[13] & 0xFF)<< 16)| 
                      ((image_data[12] & 0xFF) << 24);
        
        
        int num_of_images = (image_data[7] & 0xFF) | 
                            ((image_data[6] & 0xFF)<< 8)| 
                            ((image_data[5] & 0xFF)<< 16)| 
                            ((image_data[4] & 0xFF) << 24);
        ImageMNIST[] images = new ImageMNIST[num_of_images];
        int image_i = 0;
        for(int id_index = 16; id_index < image_data.length; id_index += rows*columns){
            byte[] pixels = new byte[rows*columns];
            for(int r = 0; r < rows; r++){
                for(int c = 0; c < columns; c++){
                    pixels[r*rows+c] = image_data[id_index+(r*rows)+c];
                }
            }
            images[image_i] = new ImageMNIST(pixels, labels[image_i]);
            image_i++;
        }
        return images;
    }
}
